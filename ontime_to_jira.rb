#!/usr/bin/env ruby
require 'byebug'
require 'httparty'
require 'tempfile'
require 'httmultiparty'
require 'tiny_tds'
require 'sanitize'

def get_ontime_data(type, access_token, ontime_base_url)
  response = HTTParty.get("#{ontime_base_url}v1/#{type}?access_token=#{access_token}")
  return response
end

def get_jira_data(url, basic_auth)
  headers = {'Content-Type' => 'application/json'}
  response = HTTParty.get(url, :basic_auth => basic_auth, :headers => headers)
  return response
end

def post_jira_data(url, body, basic_auth)
  headers = {'Content-Type' => 'application/json'}
  response = HTTParty.post(url, :basic_auth => basic_auth, :body => body.to_json, :headers => headers)
  return response
end

def post_jira_attachment(url, file, basic_auth)
  headers = {"X-Atlassian-Token" => "no-check"}
  response = HTTMultiParty.post(url, :basic_auth => basic_auth, :headers => headers, :query => {:file => File.new(file)})
  return response
end

def create_object(object, jira_project_hash, issue_result, custom_fields_object)
 jira_object = 
 {
    "fields" =>
      {"project" =>
      {
          "id" => "#{jira_project_hash["#{object["project"]["name"]}"]["id"]}"
      },
      "issuetype" =>
      {
        "id" => "#{issue_result.first["id"]}"
      },
      "summary" => "#{object["name"]}",
      "assignee" =>
      {
        "name" => "#{object["assigned_to"]["name"].strip}"
      },
      "reporter" =>
      {
        "name" => "#{object["reported_by"]["name"].strip}"
      },
      "timetracking" =>
      {
        "originalEstimate" => "#{object["estimated_duration"]["duration_minutes"]}",
        "remainingEstimate" => "#{object["remaining_duration"]["duration_minutes"]}"
      },
      "environment" => "#{object["custom_fields"]["custom_134"]}",
      "description" => "#{object["description"]}",
      "duedate" => "#{object["due_date"].to_s}",
      "#{custom_fields_object["Start Date"]["id"]}" => object["start_date"] != nil ? "#{object["start_date"][0..9]}" : "",
      "#{custom_fields_object["Completed Date"]["id"]}" => object["completion_date"] != nil ? "#{object["completion_date"][0..9]}" : "",
      "#{custom_fields_object["Is Completed"]["id"]}" => {"id" => "-1", "value" => "#{object["is_completed"]}"},
      "#{custom_fields_object["Created By"]["id"]}" => "#{object["created_by"]["name"]}",
      "#{custom_fields_object["Last Updated"]["id"]}" => "#{object["last_updated_by"]["name"]}",
      "#{custom_fields_object["Last Updated Time"]["id"]}" => object["last_updated_time"] != nil ? "#{object["last_updated_time"][0..9]}" : "",
      "#{custom_fields_object["Customer"]["id"]}" => "#{object["customer"]["company_name"]} #{object["customer"]["company_url"]}",
      "#{custom_fields_object["Ontime Custom Fields"]["id"]}" => 
        "#{object["custom_fields"]["custom_218"]} "+
        "#{object["custom_fields"]["custom_153"]} "+
        "#{object["custom_fields"]["custom_135"]} "+
        "#{object["custom_fields"]["custom_222"]} "+
        "#{object["custom_fields"]["custom_146"]} "+
        "#{object["custom_fields"]["custom_129"]} "+
        "#{object["custom_fields"]["custom_130"]} "+
        "#{object["custom_fields"]["custom_131"]} "+
        "#{object["custom_fields"]["custom_132"]} "+
        "#{object["custom_fields"]["custom_133"]} "+
        "#{object["custom_fields"]["custom_204"]} "+
        "#{object["custom_fields"]["custom_202"]} "+
        "#{object["custom_fields"]["custom_134"]} "+
        "#{object["custom_fields"]["custom_210"]} "+
        "#{object["custom_fields"]["custom_193"]}"
      }
    }
    jira_object["fields"]["assignee"].delete_if {|key, value| value == ""}
    jira_object["fields"]["reporter"].delete_if {|key, value| value == ""}
    jira_object["fields"].delete_if {|key, value| value == {}}
    jira_object["fields"].delete_if {|key, value| value == []}
    jira_object["fields"].delete_if {|key, value| value == ""}
    jira_object["fields"].delete_if {|key, value| value == " "}
    jira_object["fields"].delete_if {|key, value| value == nil}
    return jira_object
end

if File.file?("ontime_to_jira") then File.delete(File.expand_path("ontime_to_jira")) end
File.new("ontime_to_jira", "w")
basic_auth = { :username => 'davidf', :password => "PNU/Q~.3!9Ya'aCv" }
ontime_base_url = "https://ontime.mertechdata.com/OnTime2013Web/api/"
jira_base_url = "http://45.33.121.224/rest/api/latest/"
ontime_token_req = HTTParty.get("#{ontime_base_url}oauth2/token?grant_type=password&username=davidf&password=a38vOetwNfF@&client_id=874a0f77-7eb0-4ee2-8c46-4611c2149d79&client_secret=b1039462-d918-426d-a6f0-36d9847e2b05")
access_token = ontime_token_req["access_token"]
defects = get_ontime_data("defects", access_token, ontime_base_url)
features = get_ontime_data("features", access_token, ontime_base_url)
tasks = get_ontime_data("tasks", access_token, ontime_base_url)
incidents = get_ontime_data("incidents", access_token, ontime_base_url)
projects = get_ontime_data("projects", access_token, ontime_base_url)
users = get_ontime_data("users", access_token, ontime_base_url)
client = TinyTds::Client.new username: 'davidf', password: 'mertech13*', host: 'mdstwo.mertechdata.com'
jira_project_hash = Hash.new
projects["data"].each do |project|
  print "Processing Project #{project["name"]}\n"
  if not project["children"].empty?
    project["children"].each do |child|
      print "Processing Project #{child["name"]}\n"
      project_object =
      {
        "key" => "#{child["name"][0...10].upcase.gsub(/\s+/, "").gsub("/","")}",
        "name" => "#{child["name"]}",
        "projectTypeKey" => "software",
        "description" => "#{child["description"]}",
        "lead" => "Administrator",
        "url" => "http://mertech.com",
        "assigneeType" => "PROJECT_LEAD"
      }
      jira_response = post_jira_data(jira_base_url + "project", project_object, basic_auth)
      if jira_response.code != 201
        print "Project #{child["name"]} did not process\n"
        File.open("ontime_to_jira", "a+") { |f|
            f.puts("Project #{child["name"]} did not process\n")
            f.puts("Post response: " + jira_response["errors"].to_s + "\n")
            f.puts("Task object: " + child.to_s + "\n")
            f.puts("Object: " + project_object.to_s + "\n\n")
          }
      end
      project_hash = {"#{child["name"]}" => {"id" => jira_response.parsed_response["id"]}}
      jira_project_hash.merge!(project_hash)
      if not child["children"].empty?
        child["children"].each do |child2|
          print "Processing Project #{child2["name"]}\n"
          project_object =
          {
            "key" => "#{child2["name"][0...10].upcase.gsub(/\s+/, "").gsub("/","")}",
            "name" => "#{child2["name"]}",
            "projectTypeKey" => "software",
            "description" => "#{child2["description"]}",
            "lead" => "Administrator",
            "url" => "http://mertech.com",
            "assigneeType" => "PROJECT_LEAD"
          }
          jira_response = post_jira_data(jira_base_url + "project", project_object, basic_auth)
          if jira_response.code != 201
            print "Project #{child2["name"]} did not process\n"
            File.open("ontime_to_jira", "a+") { |f|
                f.puts("Project #{child2["name"]} did not process\n")
                f.puts("Post response: " + jira_response["errors"].to_s + "\n")
                f.puts("Task object: " + child2.to_s + "\n")
                f.puts("Object: " + project_object.to_s + "\n\n")
              }
          end
          project_hash = {"#{child2["name"]}" => {"id" => jira_response.parsed_response["id"]}}
          jira_project_hash.merge!(project_hash)
        end
      end
    end
  end
  project_object =
  {
    "key" => "#{project["name"][0...10].upcase.gsub(/\s+/, "").gsub("/","")}",
    "name" => "#{project["name"]}",
    "projectTypeKey" => "software",
    "description" => "#{project["description"]}",
    "lead" => "Administrator",
    "url" => "http://mertech.com",
    "assigneeType" => "PROJECT_LEAD"
  }
  jira_response = post_jira_data(jira_base_url + "project", project_object, basic_auth)
  if jira_response.code != 201
    print "Project ##{project["name"]} did not process\n"
    File.open("ontime_to_jira", "a+") { |f|
        f.puts("Project ##{project["name"]} did not process\n")
        f.puts("Post response: " + jira_response["errors"].to_s + "\n")
        f.puts("Task object: " + project.to_s + "\n")
        f.puts("Object: " + project_object.to_s + "\n\n")
      }
  end
  project_hash = {"#{project["name"]}" => {"id" => jira_response.parsed_response["id"]}}
  jira_project_hash.merge!(project_hash)
end
puts "Please Assign Fields assignee, duedate, environment, and timetracking to Projects. Press Return/Enter When Finished"
gets
users["data"].each do |user|
  print "Processing User #{user["first_name"]} #{user["last_name"]}\n"
  user_object =
  {
    "name" => "#{user["first_name"]} #{user["last_name"]}",
    "password" => "mds1234*",
    "emailAddress" => "#{user["email"]}",
    "displayName" => "#{user["login_id"]}"
  }
  jira_response = post_jira_data(jira_base_url + "user", user_object, basic_auth)
  if jira_response.code != 201
    print "User #{user["first_name"]} #{user["last_name"]} did not process\n"
    File.open("ontime_to_jira", "a+") { |f|
        f.puts("User #{user["first_name"]} #{user["last_name"]} did not process\n")
        f.puts("Post response: " + jira_response["errors"].to_s + "\n")
        f.puts("Task object: " + user.to_s + "\n")
        f.puts("Object: " + user_object.to_s + "\n\n")
      }
  end
end
issue_types = get_jira_data(jira_base_url + "issuetype", basic_auth)
issue_result = issue_types.parsed_response.select do |elem|
  elem["name"] == "Task"
end
custom_fields_object = {}
custom_field_array = ["Start Date", "Completed Date", "Is Completed", "Created By", "Last Updated", "Last Updated Time", "Ontime Custom Fields", "Customer"]
custom_field_array.each do |item|
  print "Processing Field #{item}\n"
  if item == "Is Completed" 
    custom_type = "com.atlassian.jira.plugin.system.customfieldtypes:radiobuttons"
  else
    custom_type = "com.atlassian.jira.plugin.system.customfieldtypes:textarea"
  end
  custom_object =
  {
    "name" => item,
    "description" => "OnTime Custom Field",
    "type" => custom_type
  }
  jira_response = post_jira_data(jira_base_url + "field", custom_object, basic_auth)
  if jira_response.code != 201
    print "Field #{item} did not process\n"
    File.open("ontime_to_jira", "a+") { |f|
        f.puts("Field #{item} did not process\n")
        f.puts("Post response: " + jira_response["errors"].to_s + "\n")
      }
  end
  custom_fields_object.merge!(item => {"id" => jira_response.parsed_response["id"]})
end
puts "Please Assign Custom Fields to Projects. Press Return/Enter When Finished"
gets
tasks["data"].each do |task|
  print "Processing Task ##{task["id"]}\n"
  issue_object = create_object(task, jira_project_hash, issue_result, custom_fields_object)
  jira_response = post_jira_data(jira_base_url + "issue", issue_object, basic_auth)
  if jira_response.code != 201
    print "Task ##{task["id"]} did not process\n"
    File.open("ontime_to_jira", "a+") { |f|
        f.puts("Task ##{task["id"]} did not process\n")
        f.puts("Post response: " + jira_response["errors"].to_s + "\n")
        f.puts("Task object: " + task.to_s + "\n")
        f.puts("Object: " + issue_object.to_s + "\n\n")
      }
  end
  attachments = get_ontime_data("tasks/#{task["id"]}/attachments", access_token, ontime_base_url)
  if not attachments["data"].empty?
    attachments["data"].each do |item|
      if item["file_name"] != ""
        attachment = get_ontime_data("attachments/#{item["id"]}/data", access_token, ontime_base_url)
        file = File.open(item["file_name"], 'wb') rescue nil
        if file != nil
          print "Processing Attachment ##{item["id"]}\n"
          file.write(attachment.parsed_response)
          response = post_jira_attachment("#{jira_base_url}issue/#{jira_response.parsed_response["id"]}/attachments", file, basic_auth)
          File.delete(item["file_name"])
        else
          print "Attachment ##{item["id"]} has an invalid name\n"
          File.open("ontime_to_jira", "a+") { |f|
            f.puts("Attachment ##{item["id"]} has an invalid name" + "\n\n")
          }
        end
      else
        print "Attachment ##{item["id"]} does not have a file name and cannot be processed\n"
        File.open("ontime_to_jira", "a+") { |f|
            f.puts("Attachment ##{item["id"]} does not have a file name and cannot be processed" + "\n\n")
          }
      end
    end
  end
  result = client.execute("SELECT Notes FROM dbo.Tasks WHERE TaskId = '#{task["id"]}'")
  comment_body = result.first
  if comment_body != nil
    comment_object = {"body" => "#{Sanitize.clean(comment_body["Notes"]).strip}"}
    post_jira_data(jira_base_url + "issue/#{JSON.parse(jira_response.body)["id"]}/comment", comment_object, basic_auth)
  end
end
defects["data"].each do |defect|
  print "Processing Defect ##{defect["id"]}\n"
  issue_object = create_object(defect, jira_project_hash, issue_result, custom_fields_object)
  jira_response = post_jira_data(jira_base_url + "issue", issue_object, basic_auth)
  if jira_response.code != 201
    print "Defect ##{defect["id"]} did not process\n"
    File.open("ontime_to_jira", "a+") { |f|
        f.puts("Defect ##{defect["id"]} did not process\n")
        f.puts("Post response: " + jira_response["errors"].to_s + "\n")
        f.puts("Defect object: " + defect.to_s + "\n")
        f.puts("Object: " + issue_object.to_s + "\n\n")
      }
  end
  attachments = get_ontime_data("defects/#{defect["id"]}/attachments", access_token, ontime_base_url)
  if not attachments["data"].empty?
    attachments["data"].each do |item|
      if item["file_name"] != ""
        attachment = get_ontime_data("attachments/#{item["id"]}/data", access_token, ontime_base_url)
        file = File.open(item["file_name"], 'wb') rescue nil
        if file != nil
          print "Processing Attachment ##{item["id"]}\n"
          file.write(attachment.parsed_response)
          response = post_jira_attachment("#{jira_base_url}issue/#{jira_response.parsed_response["id"]}/attachments", file, basic_auth)
          File.delete(item["file_name"])
        else
          print "Attachment ##{item["id"]} has an invalid name\n"
          File.open("ontime_to_jira", "a+") { |f|
            f.puts("Attachment ##{item["id"]} has an invalid name" + "\n\n")
          }
        end
      else
        print "Attachment ##{item["id"]} does not have a file name and cannot be processed\n"
        File.open("ontime_to_jira", "a+") { |f|
            f.puts("Attachment ##{item["id"]} does not have a file name and cannot be processed" + "\n\n")
          }
      end
    end
  end
  result = client.execute("SELECT Notes FROM dbo.Defects WHERE DefectId = '#{defect["id"]}'")
  comment_body = result.first
  if comment_body != nil
    comment_object = {"body" => "#{Sanitize.clean(comment_body["Notes"]).strip}"}
    post_jira_data(jira_base_url + "issue/#{JSON.parse(jira_response.body)["id"]}/comment", comment_object, basic_auth)
  end
end
features["data"].each do |feature|
  print "Processing Feature ##{feature["id"]}\n"
  issue_object = create_object(feature, jira_project_hash, issue_result, custom_fields_object)
  jira_response = post_jira_data(jira_base_url + "issue", issue_object, basic_auth)
  if jira_response.code != 201
    print "Feature ##{feature["id"]} did not process\n"
    File.open("ontime_to_jira", "a+") { |f|
        f.puts("Feature ##{feature["id"]} did not process\n")
        f.puts("Post response: " + jira_response["errors"].to_s + "\n")
        f.puts("Feature object: " + feature.to_s + "\n")
        f.puts("Object: " + issue_object.to_s + "\n\n")
      }
  end
  attachments = get_ontime_data("features/#{feature["id"]}/attachments", access_token, ontime_base_url)
  if not attachments["data"].empty?
    attachments["data"].each do |item|
      if item["file_name"] != ""
        attachment = get_ontime_data("attachments/#{item["id"]}/data", access_token, ontime_base_url)
        file = File.open(item["file_name"], 'wb') rescue nil
        if file != nil
          print "Processing Attachment ##{item["id"]}\n"
          file.write(attachment.parsed_response)
          response = post_jira_attachment("#{jira_base_url}issue/#{jira_response.parsed_response["id"]}/attachments", file, basic_auth)
          File.delete(item["file_name"])
        else
          print "Attachment ##{item["id"]} has an invalid name\n"
          File.open("ontime_to_jira", "a+") { |f|
            f.puts("Attachment ##{item["id"]} has an invalid name" + "\n\n")
          }
        end
      else
        print "Attachment ##{item["id"]} does not have a file name and cannot be processed\n"
        File.open("ontime_to_jira", "a+") { |f|
            f.puts("Attachment ##{item["id"]} does not have a file name and cannot be processed" + "\n\n")
          }
      end
    end
  end
  result = client.execute("SELECT Notes FROM dbo.Features WHERE FeatureId = '#{feature["id"]}'")
  comment_body = result.first
  if comment_body != nil
    comment_object = {"body" => "#{Sanitize.clean(comment_body["Notes"]).strip}"}
    post_jira_data(jira_base_url + "issue/#{JSON.parse(jira_response.body)["id"]}/comment", comment_object, basic_auth)
  end
end
incidents["data"].each do |incident|
  print "Processing Incident ##{incident["id"]}\n"
  issue_object = create_object(incident, jira_project_hash, issue_result, custom_fields_object)
  jira_response = post_jira_data(jira_base_url + "issue", issue_object, basic_auth)
  if jira_response.code != 201
    print "Incident ##{incident["id"]} did not process\n"
    File.open("ontime_to_jira", "a+") { |f|
        f.puts("Incident ##{incident["id"]} did not process\n")
        f.puts("Post response: " + jira_response["errors"].to_s + "\n")
        f.puts("Incident object: " + incident.to_s + "\n")
        f.puts("Object: " + issue_object.to_s + "\n\n")
      }
  end
  attachments = get_ontime_data("incidents/#{incident["id"]}/attachments", access_token, ontime_base_url)
  if not attachments["data"].empty?
    attachments["data"].each do |item|
      if item["file_name"] != ""
        attachment = get_ontime_data("attachments/#{item["id"]}/data", access_token, ontime_base_url)
        file = File.open(item["file_name"], 'wb') rescue nil rescue nil
        if file != nil
          print "Processing Attachment ##{item["id"]}\n"
          file.write(attachment.parsed_response)
          response = post_jira_attachment("#{jira_base_url}issue/#{jira_response.parsed_response["id"]}/attachments", file, basic_auth)
          File.delete(item["file_name"])
        else
          print "Attachment ##{item["id"]} has an invalid name\n"
          File.open("ontime_to_jira", "a+") { |f|
            f.puts("Attachment ##{item["id"]} has an invalid name" + "\n\n")
          }
        end
      else
        print "Attachment ##{item["id"]} does not have a file name and cannot be processed\n"
        File.open("ontime_to_jira", "a+") { |f|
            f.puts("Attachment ##{item["id"]} does not have a file name and cannot be processed" + "\n\n")
          }
      end
    end
  end
  result = client.execute("SELECT Notes FROM dbo.Incidents WHERE IncidentId = '#{incident["id"]}'")
  comment_body = result.first
  if comment_body != nil
    comment_object = {"body" => "#{Sanitize.clean(comment_body["Notes"]).strip}"}
    post_jira_data(jira_base_url + "issue/#{JSON.parse(jira_response.body)["id"]}/comment", comment_object, basic_auth)
  end
end